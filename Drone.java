public class Drone {

  private int x, y, droneID, dx, dy; // drone position, id, and how moves in x,y direction
  private static int droneCount = 0; // give each drone a unique number

  												
  public Drone(int bx, int by) { 
    x = bx;
    y = by;
    droneID = droneCount++;
    dx = 1;
    dy = 1;
  } 

  public Drone(String s) {
    this(0, 0);
    StringSplitter ss = new StringSplitter(s, " ");
    setXY(ss.getNthInt(0, 5), ss.getNthInt(1, 8));
  }

  public int getX() {
    return x;
  }

  public int getY() {
    return y;
  }

  public void setXY(int nx, int ny) {
    x = nx;
    y = ny;
  }

  public String toString() {
    return "Drone " + droneID + " at " + x + ", " + y + " at " + Direction.getRandomDirection();
  }

  public void tryToMove(DroneArena a) {
    int newx = x + dx;
    int newy = y + dy;
    switch (a.canMoveHere(newx, newy)) {
      case 0:
        x = newx;
        y = newy;
        break;
      case 1:
        dx = -dx;
        break;
      case 2:
        dy = -dy;
        break;
      case 3:
        dx = -dx;
        dy = -dy;
        break;
    }
  }

  public void displayDrone(ConsoleCanvas c) {
    c.showIt(x, y, 'd'); // called the showIt method to display canvas
  } 

  public String asString() { 	// this will be used for saving the data of a drone, this will write down the below statements 
    StringBuilder sb = new StringBuilder();   	
    sb.append(x);
    sb.append(";");
    sb.append(y);
    sb.append(";");
    sb.append(droneID);
    sb.append(";");
    sb.append(dx);
    sb.append(";");
    sb.append(dy);
    sb.append(";");

    return sb.toString();
  } 

  public static Drone fromString(String s) { //this will help in loading the drones, where it will read the 'saved' file info
    String arr[] = s.split(";"); //in the asString, we put a ; between each drone info, this helps in splitting the info in this method

    Drone d = new Drone(Integer.parseInt(arr[0]), Integer.parseInt(arr[1]));
    d.droneID = Integer.parseInt(arr[2]);
    d.dx = Integer.parseInt(arr[3]);
    d.dy = Integer.parseInt(arr[4]);
    return d;
  } 

  public static void main(String[] args) {
    Drone d = new Drone(5, 3);
    System.out.println(d.toString());
  }

} 
