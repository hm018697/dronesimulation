import java.util.ArrayList;
import java.util.Random;


public class DroneArena {


  private int xmax, ymax; // xmax and ymax representing the the DroneArena 
  private ArrayList<Drone> drones; //ArrayList of drones to create a variety of drones

  private DroneArena(int xmax, int ymax, ArrayList<Drone> drones) { //added ArrayList to argument for drones
    this.xmax = xmax;
    this.ymax = ymax;
    this.drones = drones; //initialize drones 
  }

  public DroneArena(String s) {
    StringSplitter ss = new StringSplitter(s, ";");
    StringSplitter sa = new StringSplitter(ss.getNth(0, "5 10"), " ");
    xmax = sa.getNthInt(0, 5);
    ymax = sa.getNthInt(1, 8);
    drones = new ArrayList<Drone>(); //defined drones 
  }

  public int getXSize() {
    return xmax;
  }

  public int getYSize() {
    return ymax;
  }

  public String toString() {
    return "Arena size " + xmax + " by " + ymax + " with " + drones.toString(); 
  }


  public void moveAllDrones() { //moving all drones 
    for (Drone d : this.drones) {
      d.tryToMove(this);
    }
  }


  public int canMoveHere(int x, int y) {
    int status = 0;
    if (x < 0 || x >= xmax) {
      status += 1;
    }
    if (y < 0 || y >= ymax) {
      status += 2;
    }
    return status;
  }

  public void addDrone() {
    Random randGen;
    randGen = new Random(); //create random 'randGen'

    boolean droneFound = false; 
    while (!droneFound) {
      int randX = randGen.nextInt(this.xmax);
      int randY = randGen.nextInt(this.ymax); //return random numbers between 0 and xmax/ymax
      boolean droneExist = false;  //droneExist to make sure drones are in unique locations
      for (Drone d : this.drones) {
        if (d.getX() == randX && d.getY() == randY) { //if the x and y of a drone == to the randX AND randY which were generated
          droneExist = true;							// , then there is an existing drone (cannot place new drone)
        }
      }
      if (!droneExist) { 				// if there is a drone that exists in the randX and randY after the above then...
        Drone d = new Drone(randX, randY); 		//adds random new drone positions 
        this.drones.add(d);					//add drone to drones (ArrayList)
        droneFound = true;
      }
    }

  }

  public void showDrones(ConsoleCanvas c) { //show drones in the display of the canvas
    for (Drone d : this.drones) { //for loop to show more than one drone
      d.displayDrone(c);
    }
  } 

  public String asString() { // this will be used for saving the data of the drone arena, this will write down the below statements 
    StringBuilder sb = new StringBuilder();
    sb.append(xmax);
    sb.append(":");
    sb.append(ymax);
    sb.append(":");
    sb.append(drones.size());
    sb.append(":");
    for (Drone d : drones) { //this will do the same as in Drone.asString, but we will collect info of multiple drones
      sb.append(d.asString());
      sb.append(":");
    } 

    return sb.toString();
  } 

  public static DroneArena fromString(String s) { //this will help in loading the dronearena, where it will read the 'saved' file info
    String[] elements = s.split(":");
    int xmax = Integer.parseInt(elements[0]);
    int ymax = Integer.parseInt(elements[1]);
    int numDrones = Integer.parseInt(elements[2]);
    ArrayList<Drone> drones = new ArrayList<>();
    for (int i = 3; i < numDrones + 3; i++) {
      Drone d = Drone.fromString(elements[i]);
      drones.add(d);
    }

    return new DroneArena(xmax, ymax, drones);
  }


  public static void main(String[] args) {
    DroneArena a = new DroneArena("20 10;16 9");
    a.addDrone();

    System.out.println(a.toString());
    for (int ct = 0; ct < 10; ct++) {

      a.moveAllDrones();
      a.addDrone();
      System.out.println(a.toString());
    }
  }

}
