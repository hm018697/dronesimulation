
public class ConsoleCanvas {

  private int x, y; 

  private char[][] canvas;

  public ConsoleCanvas(int x, int y) {

    this.x = x; //initialized canvas sizes
    this.y = y;

    canvas = new char[x + 2][y + 2];

    for (int i = 0; i < x + 2; i++) { //creating canvas size (based on arena size +2)
      for (int j = 0; j < y + 2; j++) {
        canvas[i][j] = ' ';
      }
    }

    for (int i = 0; i < x + 2; i++) { //adding '#' to the vertical lines of the canvas
      canvas[i][0] = '#'; 
      canvas[i][y + 1] = '#'; 
    }

    for (int i = 0; i < y + 2; i++) { //adding '#' to the horizontal lines of the canvas
      canvas[0][i] = '#';
      canvas[x + 1][i] = '#';
    } 


  }

  public void showIt(int sx, int sy, char sd) { // to show canvas with 'd' 
	  canvas[sx + 1][sy + 1] = sd; 
  } 

  public String toString() { // toString for displaying '#' borders to create canvas

    StringBuilder sb = new StringBuilder();
    for (int i = 0; i < x + 2; i++) {
      for (int j = 0; j < y + 2; j++) {
        sb.append(canvas[i][j]);

      }
      sb.append("\n");
    }

    return sb.toString();
  } 

  public static void main(String[] args) {
    ConsoleCanvas c = new ConsoleCanvas(5, 10); // create a canvas
    c.showIt(4, 3, 'D'); // add a Drone at 4,3
    System.out.println(c.toString()); // display result
  }
}
