import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

/**
 * Simple program to show arena with multiple drones
 *
 * @author shsmchlr
 */
public class DroneInterface {

  private Scanner s;                // scanner used for input from user
  private DroneArena myArena;        // arena in which drones are shown

  /**
   * constructor for DroneInterface sets up scanner used for input and the arena then has main loop
   * allowing user to enter commands
   */
  public DroneInterface() {
    s = new Scanner(System.in);      // set up scanner for user input

    myArena = new DroneArena("20 6;16 9");  // create arena of size 20*6

    char ch = ' ';
    do {
      System.out.print("Enter (A)dd drone, get "
          + "(I)nformation, "
          + "(D)isplay drone arena, "
          + "(M)ove all drones, "
          + "(P)lay Animation, "
          + "(N)ew Drone arena, "
          + "(S)ave Drone arena, "
          + "(L)oad Drone arena, "
          + "or e(X)it > ");
      ch = s.next().charAt(0);
      s.nextLine();
      switch (ch) {
        case 'A':
        case 'a':
          myArena.addDrone();  // add a new drone to arena
          break;
        case 'I':
        case 'i':
          System.out.println(myArena.toString());
          break;
        case 'D':
        case 'd':
          doDisplay(); // DISPLAY DRONE ARENA
          break;
        case 'M':
        case 'm':
          myArena.moveAllDrones(); //move drones
          break;
        case 'N':
        case 'n':
          System.out.print("Enter drone size (in the format 'x y;? ?') ");
          myArena = new DroneArena(s.nextLine()); // create a new drone arena
          break;
        case 'S':
        case 's':
          System.out.print("Enter file to save to: ");				
          String fileToSave = s.nextLine();
          try {
            ArrayList<String> lines = new ArrayList<>(); 
            lines.add(myArena.asString()); 
            Files.write(Paths.get(fileToSave), lines);	//saves file										
          } catch (IOException e) { 
            System.out.println("Error writing to file: " + e.getMessage()); //will give error message if there is an exception
          }
          break;
        case 'L':
        case 'l':
          System.out.print("Enter file to load from: ");
          String fileToLoad = s.nextLine();
          try {
            List<String> lines = Files.readAllLines(Paths.get(fileToLoad));
            myArena = DroneArena.fromString(lines.get(0)); //loads files
          } catch (IOException e) {
            System.out.println("Error reading from file: " + e.getMessage()); 
          }
          break; 
        case 'P':
        case 'p':
        	playAnimation();
        	break;
        case 'x':
          ch = 'X';        // when X detected program ends
          break;
      }
    } while (ch != 'X');            // test if end

    s.close();                  // close scanner
  }

  /**
   * /** Display the drone arena on the console
   */
  void doDisplay() {
    // determine the arena size
    int x = myArena.getXSize();
    int y = myArena.getYSize();

    // hence create a suitable sized ConsoleCanvas object
    ConsoleCanvas consoleCanvas = new ConsoleCanvas(x, y);

    // call showDrones suitably
    myArena.showDrones(consoleCanvas);

    // then use the ConsoleCanvas.toString method
    System.out.println(consoleCanvas);
    
    
  }
  
  void playAnimation() {
	    for(int i = 0; i < 10; i ++) {
	      try {
	        Thread.sleep(200); // this will delay for 200ms
	      } catch (InterruptedException ignored) { } //after the delay, to do the statements below..
	      myArena.moveAllDrones();
	      doDisplay();
	    }
	  }


  public static void main(String[] args) {
    DroneInterface r = new DroneInterface();  // just call the interface
  }

} 
