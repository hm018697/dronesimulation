import java.util.Random;


    public enum Direction { 
        NORTH,
        WEST,
        EAST,
        SOUTH;

   
        public static Direction getRandomDirection() { // method to get a random direction from Direction enum
            Random random = new Random();
            return values()[random.nextInt(values().length)];
        }
    }
    
